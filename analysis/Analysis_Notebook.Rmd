---
title: "RNAseq Analysis Notebook"
author: "Spencer Nystrom"
output: html_notebook
---


```{r setup}
library(here)

# Load config file
source(here("config.R"))

# Import data from a run of the pipeline
# using relative file path ensures data will be found in correct location!
data_dir <- here("results/2017-08-22_17.29.32/")
rda_files <- list.files(data_dir, pattern = "*.rda", full = T)
lapply(rda_files, load, .GlobalEnv)
```

```{r, eval=F}
ggplot(peaks, aes(position, signal)) +
     geom_line(aes(color = behavior)) +
     scale_fill_manual(values = peak_behavior_colors)
```
