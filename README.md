# Example Project Structure
## Spencer Nystrom

**WARNING:** This project was created to be a quick example of a hypothetical genomics analysis. The code is cobbled together from multiple sources and probably none of it will run or is valid. Empty files, etc. are all just for demonstration purposes. File paths won't always be consistent either.


Description of the project goes here

Subheaders for relevant things in project

### Directories

Could be useful to include descriptions of each directory here if complicated

### Data

How samples were generated/things to consider about data (could also include detailed README's inside each data directory if needed)

### Scripts

Can be useful to describe scripts
